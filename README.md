ML-Policy: Unofficial Policy for Debian & Machine Learning
==========================================================

* [Policy Source (.rst)](./ML-Policy.rst)
* [Policy in PDF format (Latest Release)](./ML-Policy.pdf)
* [Policy in HTML format (Latest Release)](./ML-Policy.html)

See Also
--------

* [Deep Learning & Debian Development -- Mo Zhou](https://people.debian.org/~lumin/debian-dl.html)

Contributing
------------

Please [contact the maintainer](mailto:lumin@debian.org) before making changes
to this documentaiton.

License
-------

Copyright (C) 2019-2020, Mo Zhou <lumin@debian.org>

This project is licensed under CC-BY-SA-4.0 License.
