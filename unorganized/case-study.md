Case Study / Examples
=====================

https://codesearch.debian.net/search?q=%28%3Fi%29machine%5Cs*learning&literal=0&perpkg=1

TODO
----

[tesseract-ocr: please provide source for language files](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=699609)

1. [nltk-data](https://github.com/nltk/nltk_data) This can enter our main
   section after removing some components.

Applications: Computer Vision
-----------------------------

[FFmpeg Super Resolution Filter](https://ffmpeg.org/pipermail/ffmpeg-devel/2018-July/231828.html)
scales up images while completing details within it, which performs much
better than traditional algorithms such as linear upsampling. The output
of such model is expected to be consumed by human without involving in any
critical task.

[GauGan](https://blogs.nvidia.com/blog/2019/03/18/gaugan-photorealistic-landscapes-nvidia-research/)
[(demo)](http://52.12.58.174/) learns the original data distribution and
genrate realistic images according the the sketch provided by the user. It may
assist artists for picture creation. This model is not expected to perform
critical task.

Applications: Computational Linguistics
---------------------------------------


Applications: Other
-------------------

[Input methods] doesn't perform critical task. They are fine.

Academical Resource: Computer Vision
------------------------------------

[ImageNet](http://www.image-net.org/) is a well-known academical computer
vision dataset. The dataset is [non-free](http://image-net.org/download-faq).
That means the commonly used Pre-trained Deep Convolutional Neural Networks
such as AlexNet, VGG, InceptionNet, ResNet, DenseNet, EfficientNet, etc,
are not Free Models.

Academical Resource: Computational Linguistics
----------------------------------------------

such as BERT, XLNET, GPT2, ELMo

Academical Resource: Other
--------------------------


